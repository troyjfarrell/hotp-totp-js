HOTP and TOTP Demonstration
===========================

This repository contains a demonstration of the one‐time password algorithms
described in `RFC 4226`_ and `RFC 6238`_.  You may recognize these as the logic
behind `Google Authenticator`_.  The algorithms were developed as part of the
`Initiative for Open Authentication`_.

This code is available under the terms of the BSD 2-Clause License.  See the COPYING file for details.

This repository includes a copy of the `Stanford JavaScript Crypto Library`_.  It has its own licensing terms.  See the components/sjcl/README/COPYRIGHT file for details.

.. _`RFC 4226`: http://tools.ietf.org/html/rfc4226
.. _`RFC 6238`: http://tools.ietf.org/html/rfc6238
.. _`Google Authenticator`: https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2
.. _`Initiative for Open Authentication`: http://www.openauthentication.org/
.. _`Stanford JavaScript Crypto Library`: https://crypto.stanford.edu/sjcl/
